dimension seconds/evaluations
  -----------------------------
      2      2.8e-04 
      3      2.7e-04 
      5      2.8e-04 
     10      2.7e-04 
     20      4.8e-03 
  -----------------------------
  bbob-biobj done (4125 of 4125 problems benchmarked), Wed Oct 17 21:56:56 2018 (0h14:49 total elapsed time).
Data written to folder exdata/adaptiveHVIBEA_on_bbob-biobj_budget0100xD
To post-process the data call 
    python -m cocopp exdata/adaptiveHVIBEA_on_bbob-biobj_budget0100xD 
from a system shell or 
    cocopp.main("exdata/adaptiveHVIBEA_on_bbob-biobj_budget0100xD") 
from a python shell

```
{
    "alpha" : 20,
    "mu" : 10,
    "dim" : 2,
    "kappa" : 0.05,
    "rho" : 2,
    "max_generations" : 10000,
    "n_offsprings" : 2,
    "tournament" : 2,
    "matingProba" : 0.9,
    "mutationProba" : 0.01  
}

```
