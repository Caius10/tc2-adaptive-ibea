#binary tournament selection
import random
import IBEA as ea
import numpy as np
class TournementSelection: 
	def __init__(self,tournament_size=2,groupe_size=2) : 
		# Selection operator using Tournament Strategy with tournament size equals to two by default.
		self.groupe_size = groupe_size
		self.tournament_size = tournament_size

	def max_of_groupe(self,group, parent = 1) : 
		# return the index of the maximum element in the groupe 
		index = 0
		max_fitness = group[0].fitness
		for i in range(len(group)) :
			if(group[i].fitness > max_fitness and parent != group[i].fitness) : 
				index = i
				max_fitness = group[i].fitness
		return index
	
	def select(self,population,mu) : 
    	# Select a pair of parent using Tournament strategy.
		pool = []
		i = 0
		while i < (mu/2) : 
			if self.tournament_size >= len(population) or 2*self.groupe_size>len(population):
				msg = 'Tournament size ({}) or two groups size ({}) are larger than population size ({})'
				raise ValueError(msg.format(self.tournament_size,self.groupe_size,len(population)))
			groupe1 = random.sample(population, self.groupe_size)
			groupe2 = random.sample(population, self.groupe_size)
			father_index = self.max_of_groupe(groupe1)
			mother_index = self.max_of_groupe(groupe2, groupe1[father_index].fitness)
			father = groupe1[father_index]
			mother = groupe2[mother_index]
			pool.append([father,mother])
			i+=1
		
		return pool
    
 

# a  simulated binary crossover (SBX) operator has  been developed to solve problems having  continuous search space.  
# article 4 
def recombination_SBX(pool,ibea,alfa=0.8,eta=1) : 
		#population
		#alfa : cross probability 
		#eta : the parameter of SBX (value 1 is recommanded) 
		offsprings = []
		for j in range(len(pool)):
			r = random.random()
			bq = np.zeros(ibea.sizeInd)
			if r < alfa : 
				randList = np.random.random(ibea.sizeInd)
				yes_no = (randList<=0.5)
				for i in range(ibea.sizeInd) :
					if yes_no[i] : 
						bq[i] = (2.0*randList[i])**(1.0/(eta+1.0))
					else :
						bq[i] = (1.0/(2.0-2.0*randList[i]))**(1.0/(eta+1.0))
				chromosome_father = pool[j][0].values
				chromosome_mother = pool[j][1].values
				chromosome_son1 = 0.5*((1.0+bq)*chromosome_father+(1.0-bq)*chromosome_mother)
				chromosome_son2 = 0.5*((1.0-bq)*chromosome_father+(1.0+bq)*chromosome_mother)
				# bound validator 
				chromosome_son1 = np.max(np.vstack((chromosome_son1,np.array([ibea.lower_bounds] * ibea.sizeInd))),0)
				chromosome_son1 = np.min(np.vstack((chromosome_son1,np.array([ibea.upper_bounds] * ibea.sizeInd))),0)
				chromosome_son2 = np.max(np.vstack((chromosome_son2,np.array([ibea.lower_bounds] * 	ibea.sizeInd))),0)
				chromosome_son2 = np.min(np.vstack((chromosome_son2,np.array([ibea.upper_bounds] * ibea.sizeInd))),0)
				son1 = ea.Individual(ibea)
				son2 = ea.Individual(ibea)
				son1.values = chromosome_son1
				son2.values = chromosome_son2
				son1.objectiveFunction = ibea.fun(chromosome_son1)
				son2.objectiveFunction = ibea.fun(chromosome_son2)
				offsprings.append(son1)
				offsprings.append(son2)
				ihypervolumes = np.array([ibea.IHypervolume(ibea.lpopulation[i], add,recompute=1) 
				for i in range(len(ibea.lpopulation))
				for add in [son1,son2]])
				ibea.max_ihypervolume = max(ibea.max_ihypervolume,ihypervolumes.max())
		return offsprings
	


def mutation(ibea, eta_m=1):
    # population
    for i in xrange(len(ibea.lpopulation)):
        individu = ibea.lpopulation[i]
        values = individu.values
        for j in xrange(ibea.sizeInd):
            r=random.random()
            if r<=ibea.mutationProba:
                y=values[j]
                ylow=ibea.lower_bounds[j]
                yup=ibea.upper_bounds[j]
                delta1=1.0*(y-ylow)/(yup-ylow)
                delta2=1.0*(yup-y)/(yup-ylow)
                u=random.random()
                mut_pow=1.0/(eta_m+1.0)
                if u<=0.5:
                    xy=1.0-delta1
                    val=2.0*u+(1.0-2.0*u)*(xy**(eta_m+1.0))
                    deltaq=val**mut_pow-1.0
                else:
                    xy=1.0-delta2
                    val=2.0*(1.0-u)+2.0*(u-0.5)*(xy**(eta_m+1.0))
                    deltaq=1.0-val**mut_pow
                y=y+deltaq*(yup-ylow)
                y=min(yup, max(y, ylow))
                individu.values[j] = y