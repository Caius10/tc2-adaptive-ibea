import random
import IBEA as ea
import numpy as np
import crossover
#polynomial mutation of an individual
def mutation(ibea, pmut, eta_m=1):
    # population 
    # pmut : the probability of mutation
    for i in xrange(len(ibea.lpopulation)):
        individu = ibea.lpopulation[i]
        values = individu.values
        for j in xrange(ibea.sizeInd):
            r=random.random()
            if r<=pmut:
                y=values[j]
                ylow=ibea.lower_bounds
                yup=ibea.upper_bounds
                delta1=1.0*(y-ylow)/(yup-ylow)
                delta2=1.0*(yup-y)/(yup-ylow)
                u=random.random()
                mut_pow=1.0/(eta_m+1.0)
                if u<=0.5:
                    xy=1.0-delta1
                    val=2.0*u+(1.0-2.0*u)*(xy**(eta_m+1.0))
                    deltaq=val**mut_pow-1.0
                else:
                    xy=1.0-delta2
                    val=2.0*(1.0-u)+2.0*(u-0.5)*(xy**(eta_m+1.0))
                    deltaq=1.0-val**mut_pow
                y=y+deltaq*(yup-ylow)
                y=min(yup, max(y, ylow))
                individu.values[j] = y


# test mutation
def main():
    ibea = ea.IBEA()
    print("parameters : " + str({"alpha": ibea.alpha, "mu": ibea.mu, "n_offsprings": ibea.n_offsprings, "dim": ibea.dim,
    "kappa" : ibea.kappa,"rho" : ibea.rho,"max_generations" : ibea.max_generations}))
    pop = ibea.initializePop()
    print("taille init pop : %i" % len(pop))
    ibea.changeIndividual(pop[2], 4*np.ones(pop[2].sizeInd), 18*np.ones(pop[2].sizeInd), 870)
    pop[2].printIndividual()
    pop2 = pop[:]

    ibea.computeFitness()

    #adding individual for P to be bigger than alpha
    ind1 = ea.Individual(ibea, 56960)
    ind2 = ea.Individual(ibea, 68465)
    ibea.addIndividual(ind1)
    ibea.addIndividual(ind2)

    print("taille apres ajout : %i" % len(ibea.lpopulation))
    for ind in ibea.lpopulation: 
        ind.fitness = np.random.uniform(ibea.lower_bounds, ibea.upper_bounds)
        print("fitness %i %f" % (ind.getIndexInd(ibea),ind.fitness))
    ibea.environmentalSelection()
    # mating pool
    tournement = crossover.TournementSelection()
    pop = ibea.lpopulation[:]
    pool = tournement.select(pop,ibea.mu)
 
    # recombination
    offsprings = crossover.recombination_SBX(pool,ibea)
    for son in offsprings : 
        print(son.values)
        ibea.lpopulation.append(son)  
    print(ibea.lpopulation[0].values)
    mutation(ibea, pmut = 0.2)
    print(ibea.lpopulation[0].values)


if __name__ == '__main__':
    main()


