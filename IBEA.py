# coding=utf-8

import numpy as np
import json
import math
import crossover 
import mutation
import time
np.random.seed()

class IBEA:
    """
        Attributes:
            alpha           Population size
            mu              Size of the mating pool
            n_offsprings    Number of offspring individuals
            dim             Dim of objective
            kappa           Fitness scaling ratio
            rho             Reference point for the hypervolume calculation
            max_generations Maximum number of generations
            m               Generation counter
            lmatingPool     List of parents
            lpopulation     List of individuals
            solution        ?
    """


    def __init__(self, filename = "param.json"):
        """
            Algorithme to initiate parameters

                alpha           Population size
                mu              Size of the mating pool
                n_offsprings    Number of offspring individuals
                dim             Dim of objective
                kappa           Fitness scaling ratio
                rho             Reference point for the hypervolume calculation
                max_generations Maximum number of generations
                tournament      Tournament size for mating selection
        
        Keyword Arguments:
            filename {str} -- [Json file with following parameters] (default: {"param.json"})
        """

        with open(filename) as json_param:
            params = json.load(json_param)
        self.alpha = params["alpha"]
        self.mu = params["mu"]
        self.n_offsprings = params["n_offsprings"]
        self.dim = params["dim"]
        self.kappa = params["kappa"]
        self.rho = params["rho"]
        self.max_generations = params["max_generations"]
        self.tournement = params["tournament"]
        self.mutationProba = params["mutationProba"]
        self.matingProba = params["matingProba"]
        self.ihypervolumes = np.empty(0)
        self.hypervolumes = np.empty(0)
        self.max_ihypervolume = 1
        self.m = 0
        self.lpopulation = [] #list of individuals
        self.lmatingPool = []
        self.offsprings =  []
        self.lower_bounds = 0
        self.upper_bounds = 0
        self.solution = np.empty(0)
        self.bounds = dict()
        self.fun = None
        self.sizeInd = 2 #TODO set fun.dimension
    
    def setBounds(self, lower_bounds, upper_bounds):
        self.lower_bounds = lower_bounds
        self.upper_bounds = upper_bounds

    def adaptiveHVIBEA(self, fun, lower_bounds, upper_bounds, remaining_evals):
        self.fun = fun
        self.sizeInd = fun.dimension
        self.setBounds(lower_bounds,upper_bounds)
        #step1 generate an initial population ans set counter to 0
        self.m = 0
        self.initializePop()
        remaining_evals -= self.alpha
        while self.m <= self.max_generations:
            #step 2 compute fitness
            self.calcfBounds()
            self.computeFitness()
            #step 3 environmental selection
            self.environmentalSelection()
            #step 4 : case m = N, environmental selection before returning A
            if (self.m == self.max_generations) or (remaining_evals <= self.alpha + 2 * self.n_offsprings):
                break
            #step 5
            # mating pool and recombination
            tournement = crossover.TournementSelection(tournament_size=self.tournement, groupe_size=self.n_offsprings)
            self.lmatingPool = tournement.select(self.lpopulation,self.mu)
            self.offsprings = crossover.recombination_SBX(self.lmatingPool,self,self.matingProba)
            remaining_evals -= len(self.offsprings)
            self.lpopulation.extend(self.offsprings)
            #step 6 mutation
            crossover.mutation(self,self.mutationProba)
            self.m += 1
            remaining_evals -= 1
        return self.getAllIndividualValues()
    # def adaptiveHVIBEA(self, fun, lower_bounds, upper_bounds, remaining_evals):
    #     self.fun = fun
    #     self.sizeInd = fun.dimension
    #     self.setBounds(lower_bounds,upper_bounds)
    #     #step1 generate an initial population ans set counter to 0
    #     self.m = 0
    #     self.initializePop()
    #     while self.m <= self.max_generations:
    #         #step 2 compute fitness 
    #         start2 = time.clock()
    #         self.calcfBounds()
    #         elapsed = (time.clock() - start2)
    #         print("calcfBounds used:",elapsed)
    #         start1 = time.clock()
    #         self.computeFitness()
    #         elapsed = (time.clock() - start1)
    #         print("compute Fitness used:",elapsed)
    #         start3 = time.clock()
    #         #step 3 environmental selection
    #         self.environmentalSelection()
    #         elapsed = (time.clock() - start3)
    #         print("environmentalSelection used:",elapsed)
    #         #step 4 : case m = N, environmental selection before returning A
    #         if (self.m == self.max_generations) or (remaining_evals <= 0):
    #             break
    #         start4 = time.clock()
    #         #step 5
    #         # mating pool and recombination
    #         tournement = crossover.TournementSelection(tournament_size=self.tournement, groupe_size=self.n_offsprings)
    #         self.lmatingPool = tournement.select(self.lpopulation,self.mu)
    #         self.offsprings = crossover.recombination_SBX(self.lmatingPool,self,self.matingProba)
    #         self.lpopulation.extend(self.offsprings)
    #         #step 6 mutation
    #         crossover.mutation(self,self.mutationProba)
    #         elapsed = (time.clock() - start4)
    #         print("Min used:",elapsed)
    #         self.m += 1
    #         remaining_evals -= 1
    #     return self.getAllIndividualValues()

    #return an array of individuals
    def initializePop(self):
        self.lpopulation = []
        for i in range(self.alpha):
            ind = Individual(self)
            self.lpopulation.append(ind)
        return self.lpopulation
    
    def addIndividual(self, ind):
        self.lpopulation.append(ind)
    
    def removeIndividual(self, index):
        self.lpopulation.pop(index)
    
    def getAllIndividualValues(self):
        return [ind.values for ind in self.lpopulation]
    
    #TODO : change it or remove depending on COCO
    def evaluateWithFunctions(self, lfunc):
        for ind in self.lpopulation:
            for i in range (len(lfunc)):
                ind.objectiveFunction[i] = lfunc[i](ind.values)

    #step 2
    def computeFitness(self):
        N = len(self.lpopulation)
        # precompute hypervolumes (reset values)
        self.hypervolumes = np.zeros(N)
        self.ihypervolumes = np.zeros((N,N))
        for i in range(N):
            self.hypervolumes[i] = self.Hypervolume2D(self.lpopulation[i].objectiveFunction)

        self.max_ihypervolume = max([abs(self.IHypervolume(self.lpopulation[x2],self.lpopulation[x1],[x2,x1])) 
        for x1 in range(len(self.lpopulation))
        for x2 in range(len(self.lpopulation)) 
        if x1 != x2])

        for x1 in range(N):
            #list of elements of the sum : I(x, self)
            lexp = 0
            for x2 in range(N):
                if (x2!=x1):
                    lexp += (-1)*math.exp((-1)*self.IHypervolume(self.lpopulation[x2],self.lpopulation[x1],[x2,x1])/(self.kappa * self.max_ihypervolume))
            self.lpopulation[x1].fitness = lexp

    def IHypervolume(self, ind_a, ind_b, index = [], recompute = 0):
        """ I_HD-indicator measures the volume of the objective space that is dominated by B
            but not by A with the respect to the reference point z
        Arguments:
            ind_a {list} -- list of individuals ind_a
            ind_b {Individual} -- individual ind_b
            index {list} -- index of ind_a, ind_b
            recompute {Boolean} -- if true recompute the Ihypervolume

        Returns:
            double -- I_HD-indicator(ind_a,ind_b)
        """
        # check if the indicator is already computed
        if (recompute == 1):
            if (ind_a.dominates(ind_b)):
                I = self.Hypervolume2D(ind_b.objectiveFunction) - self.Hypervolume2D(ind_a.objectiveFunction)
            else :
                I = self.Hypervolume2D(ind_a.objectiveFunction, ind_b.objectiveFunction) - self.Hypervolume2D(ind_a.objectiveFunction)
            return I
        I = self.ihypervolumes[index[0],index[1]]
        if (I !=0):
            return I    
        # compute hypervolume of all individual
        if (ind_a.equals(ind_b)):
            return 0
        elif (ind_a.dominates(ind_b)):
            I = self.Hypervolume(index[1], recompute) - self.Hypervolume(index[0],recompute)
            self.ihypervolumes[index[0],index[1]] = I
            self.ihypervolumes[index[1],index[0]] = -I
        else :
            I = self.Hypervolume2D(ind_a.objectiveFunction, ind_b.objectiveFunction) - self.Hypervolume(index[0],recompute)
            self.ihypervolumes[index[0],index[1]] = I
        return I

    def Hypervolume(self, index_a, recompute = 0):
        """ Hypervolume gives the volume of the bi-objective space dominated by individual a but not b
        Arguments:
            index_a {int} -- index of the individual
            recompute {Boolean} -- if true recompute the hypervolume
        
        Returns:
            double -- the volume of the objective space dominated by ind
        """
        if (recompute == 1):
            return self.Hypervolume2D(self.lpopulation[index_a].objectiveFunction)
        hv = self.hypervolumes[index_a]
        if (hv != 0 and recompute ==0) :
            return hv
        else :
            return self.Hypervolume2D(self.lpopulation[index_a].objectiveFunction)
        
    def Hypervolume2D(self, f_a, f_b = np.empty(0)):
        """ Hypervolume gives the volume of the bi-objective space dominated by individual a but not b
        Arguments:
            f_a {array} -- objective values of ind_a
            f_b {array} -- objective values of ind_b
        
        Returns:
            double -- the volume of the objective space dominated by ind
        """
        f_a = self.norm(f_a)
        if (len(f_b) == 0):
            f_b = self.rho * np.ones(2)
        else:
            f_b = self.norm(f_b)
        hv = 0
        if (f_a[1] < f_b[1]) :
            hv = (self.rho - f_a[1]) * (f_b[0] - f_a[0])
            hv += (self.rho - f_b[1]) * (self.rho - f_b[0])
        else :
            hv = (self.rho - f_b[1]) * (f_a[0] - f_b[0])
            hv += (self.rho - f_a[1]) * (self.rho - f_a[0])
        return hv


    def norm(self,f):
            return (f - self.bounds["min"]) / (self.bounds["max"]-self.bounds["min"])

    def calcfBounds(self):
        """
            Compute objectives values min and max bounds
        """
        N = len(self.lpopulation)
        objectiveValues = np.zeros((N,self.dim))
        for i in range(N):
            objectiveValues[i] = self.lpopulation[i].objectiveFunction
        self.bounds["min"] = np.min(objectiveValues,axis=0)
        self.bounds["max"] = np.max(objectiveValues,axis=0)

    #step 3
    def environmentalSelection(self):
        N = len(self.lpopulation)
        while N > self.alpha:
            #choose x* with the smallest fitness value
            lfitness = [ind.fitness for ind in self.lpopulation]
            i_min = lfitness.index(min(lfitness))
            #store x* to use it when updating the fitness of the remaining individuals
            xStar = self.lpopulation[i_min]
            #remove it 
            self.calcfBounds()
            self.removeIndividual(i_min)
            N = N -1
            #update fitness for all remaining individuals
            
            for i in range(N - 1):
                self.lpopulation[i].fitness += math.exp((-1)*self.IHypervolume(xStar, self.lpopulation[i], recompute= 1)/(self.kappa * 1.0)) 


#d dimension solutions
class Individual:
    def __init__(self, ibea, fitness = 0):
        self.fitness = fitness
        self.sizeInd = ibea.sizeInd
        self.values = np.random.uniform(ibea.lower_bounds, ibea.upper_bounds, self.sizeInd)
        self.objectiveFunction = ibea.fun(np.array(self.values))
        
    def printIndividual(self):
        print({'values' : self.values, 'objective' : self.objectiveFunction, 'fitness' : self.fitness})

    def getIndexInd(self, ibea):
        return [np.array_equal(self.values, x.values) for x in ibea.lpopulation].index(True)

    def setValues(self, a_values, a_obj, fitness):
        self.values = a_values
        self.objectiveFunction = a_obj
        self.fitness = fitness

    def equals(self, ind):
        return np.array_equal(self.values,ind.values)

    def dominates(self, ind):
        """ Test if self dominates ind
        
        Arguments:
            ind {Individual} -- individual
        
        Returns:
            Boolean -- True if self dominates ind else False
        """
        strict_dominance = False
        for i in range(len(self.objectiveFunction)):
            # if one f 
            if self.objectiveFunction[i] > ind.objectiveFunction[i]:
                return False
            elif self.objectiveFunction[i] < ind.objectiveFunction[i]:
                    strict_dominance = True
        return strict_dominance